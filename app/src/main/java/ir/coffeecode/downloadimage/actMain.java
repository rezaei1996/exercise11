package ir.coffeecode.downloadimage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;

public class actMain extends AppCompatActivity {

    RealmResults<ImageList> imgList;
    ImageList[] lstResult;
    RequestQueue queue;
    public final String Url = "http://razmkhah.ir/androidsamen/getPics";
    Realm realm;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        //Realm getDefaultInstance

        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);

        realm = Realm.getDefaultInstance();

        lstImages();

        imgList = realm.where(ImageList.class).findAll();
        queue = Volley.newRequestQueue(this);

        myAdapter adapter = new myAdapter();
        ListView lst = (ListView) findViewById(R.id.lst);
        lst.setAdapter(adapter);

        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    public void lstImages() {

        StringRequest reqListImg = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                JsonObject jo = new JsonParser().parse(response).getAsJsonObject();
                JsonArray jsonArray = jo.getAsJsonArray("news");
                lstResult = new ImageList[jsonArray.size()];
                lstResult = gson.fromJson(jsonArray, ImageList[].class);
                for (int i = 0; i < lstResult.length ; i++){
                    realm.beginTransaction();
                    realm.copyToRealm(lstResult[i]);
                    realm.commitTransaction();
                    Toast.makeText(actMain.this, "Saved Data", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(actMain.this, "Error: " + error, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("apiKey", "1631f6b1e8f58b3327bf5cec2bb809f43843153c");
                return params;
            }
        };
        queue.add(reqListImg);
    }


    public String imgDownload(final ImageList img) {
        ImageRequest reqImage = new ImageRequest(img.getUrl(), new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
                path = absolutePath + img.getName() + ".JPEG";
                File file = new File(path);
                try {
                    FileOutputStream fOut = new FileOutputStream(file);
                    response.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.close();
                    //Bitmap bitmap = BitmapFactory.decodeFile(path);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 500, 500, Bitmap.Config.ARGB_4444, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(actMain.this, "Error: " + error, Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(reqImage);
        return path;
    }


    public class myAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imgList.size();
        }

        @Override
        public Object getItem(int i) {
            return imgList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater inflater = getLayoutInflater();
            View lstRow = inflater.inflate(R.layout.lst_row, viewGroup, false);
            TextView txvName = (TextView) lstRow.findViewById(R.id.txv);
            txvName.setText(imgList.get(i).getName());
            return lstRow;
        }
    }
}
