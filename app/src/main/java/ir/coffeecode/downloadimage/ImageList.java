package ir.coffeecode.downloadimage;


import io.realm.RealmObject;

/**
 * Created by Developer on 10/29/2016.
 */

public class ImageList extends RealmObject {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
